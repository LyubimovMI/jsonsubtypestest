﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using JsonSubTypes;
using Newtonsoft.Json;

namespace jsonpoly
{
    class Program
    {
        static void Main(string[] args)
        {
            IDspEventMessage bidEventMsg = new BidEventMessage()
            {
                BidRequest = new BidRequest()
                {
                    Body = "somebody"
                },
                Cancelled = true,
                BidLogs = new List<BidLog>()
                {
                    new BidLog()
                    {
                        Bid = 2.1,
                        InterestedCategory = new InterestedCategory() { PartnerCategoryId = 1 }
                    },
                    new BidLog()
                    {
                        Bid = 3.1,
                        InterestedCategory = new InterestedCategory() { PartnerCategoryId = 2 }
                    }
                }
            };

            IDspEventMessage WinNoticeMsg = new WinNotice()
            {
                Timestamp = DateTime.UtcNow,
                Visitor = "visitorFromWinNotice"
            };

            var settings = new JsonSerializerSettings()
            {
                TypeNameHandling = TypeNameHandling.All,
                ConstructorHandling = ConstructorHandling.AllowNonPublicDefaultConstructor,
                ObjectCreationHandling = ObjectCreationHandling.Auto
            };
            settings.Converters.Add(JsonSubtypesConverterBuilder
                .Of(typeof(IDspEventMessage), "Type")
                .RegisterSubtype(typeof(BidEventMessage), DspType.BidEventMessage)
                .RegisterSubtype(typeof(WinNotice), DspType.WinNotice)
                .SerializeDiscriminatorProperty()
                .Build());

            var outputString1 = JsonConvert.SerializeObject(bidEventMsg, settings);
            var outputString2 = JsonConvert.SerializeObject(WinNoticeMsg, settings);

            var output1 = JsonConvert.DeserializeObject<IDspEventMessage>(outputString1, settings);
            var output2 = JsonConvert.DeserializeObject<IDspEventMessage>(outputString2, settings);


            Console.WriteLine(outputString1);
            Console.WriteLine(output1.GetType());

            Console.WriteLine(outputString2);
            Console.WriteLine(output2.GetType());
        }
    }

    enum DspType
    {
        BidEventMessage = 1,
        WinNotice = 2
    }

    interface IDspEventMessage
    {
        DateTime Timestamp { get; }

        string Visitor { get; }
    }

    class BidRequest
    {
        public string Body { get; set; }
    }

    class WinNotice : IDspEventMessage
    {
        public DateTime Timestamp { get; set; }

        public string Visitor { get; set; } 
    }

    class BidEventMessage : IDspEventMessage
    {
        public BidRequest BidRequest { get; set; }
        public bool Cancelled { get; set; }

        public IEnumerable<BidLog> BidLogs { get; set; }

        public DateTime Timestamp { get; set; }

        public string Visitor { get; set; } 
    }

    class BidLog
    {
        public double Bid { get; set; }
        public InterestedCategory InterestedCategory { get; set; }
    }

    class InterestedCategory
    {
        public int PartnerCategoryId { get; set; }
    }
}
